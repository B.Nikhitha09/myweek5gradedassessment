WEEK5 GRADED ASSESSMENT:


mysql> create database travel;
Query OK, 1 row affected (0.01 sec)

mysql> use travel;
Database changed
mysql> create table PASSENGER(Passenger_name varchar(20),Category varchar(20),Gender varchar(20),Boarding_City varchar(20),Destination_City varchar(20),Distance int,Bus_Type varchar(20));
Query OK, 0 rows affected (0.08 sec)

mysql> desc passenger;
+------------------+-------------+------+-----+---------+-------+
| Field            | Type        | Null | Key | Default | Extra |
+------------------+-------------+------+-----+---------+-------+
| Passenger_name   | varchar(20) | YES  |     | NULL    |       |
| Category         | varchar(20) | YES  |     | NULL    |       |
| Gender           | varchar(20) | YES  |     | NULL    |       |
| Boarding_City    | varchar(20) | YES  |     | NULL    |       |
| Destination_City | varchar(20) | YES  |     | NULL    |       |
| Distance         | int         | YES  |     | NULL    |       |
| Bus_Type         | varchar(20) | YES  |     | NULL    |       |
+------------------+-------------+------+-----+---------+-------+
7 rows in set (0.01 sec)

mysql> create table Price (Bus_Type varchar(20),Distance int,Price int);
Query OK, 0 rows affected (0.03 sec)

mysql> desc price;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| Bus_Type | varchar(20) | YES  |     | NULL    |       |
| Distance | int         | YES  |     | NULL    |       |
| Price    | int         | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
3 rows in set (0.01 sec)

mysql> insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
Query OK, 1 row affected (0.01 sec)

mysql> insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper'),('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting'),('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
Query OK, 2 rows affected (0.01 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting'),('Piyush','AC','M','Pune','Nagpur',700,'Sitting');
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> select * from passenger;
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Hemant         | Non-AC   | M      | panaji        | Mumbai           |      700 | Sleeper  |
| Manish         | Non-AC   | M      | Hyderabad     | Bengaluru        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+
9 rows in set (0.00 sec)

mysql> insert into price values('Sleeper',350,770),('Sleeper',500,1100),('Sleeper',600,1320),('Sleeper',700,1540),('Sleeper',1000,2200),('Sleeper',1200,2640),('Sleeper',350,434),('Sitting',500,620),('Sitting',500,620),('Sitting',600,744),('Sitting',700,868),('Sitting',1000,1240),('Sitting',1200,1488),('Sitting',1500,1860);
Query OK, 14 rows affected (0.02 sec)
Records: 14  Duplicates: 0  Warnings: 0

mysql> select * from price;
+----------+----------+-------+
| Bus_Type | Distance | Price |
+----------+----------+-------+
| Sleeper  |      350 |   770 |
| Sleeper  |      500 |  1100 |
| Sleeper  |      600 |  1320 |
| Sleeper  |      700 |  1540 |
| Sleeper  |     1000 |  2200 |
| Sleeper  |     1200 |  2640 |
| Sleeper  |      350 |   434 |
| Sitting  |      500 |   620 |
| Sitting  |      500 |   620 |
| Sitting  |      600 |   744 |
| Sitting  |      700 |   868 |
| Sitting  |     1000 |  1240 |
| Sitting  |     1200 |  1488 |
| Sitting  |     1500 |  1860 |
+----------+----------+-------+
14 rows in set (0.00 sec)



1.How many females and how many male passengers travelled for a minimum distance of 600 KM s?

mysql> select COUNT(Case WHEN (Gender) = 'M' then 1 END) Male,COUNT(Case WHEN (Gender) = 'F' then 1 END) Female from Passenger where Distance>=600;
+------+--------+
| Male | Female |
+------+--------+
|    4 |      2 |
+------+--------+
1 row in set (0.00 sec)

2.Find the minimum ticket price for Sleeper Bus.

mysql> select min(price) from price where Bus_type = 'sleeper';
+------------+
| min(price) |
+------------+
|        434 |
+------------+
1 row in set (0.00 sec)

3.Select passenger names whose names start with character 'S'.

mysql> select * from passenger where passenger_name like 'S%';
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
+----------------+----------+--------+---------------+------------------+----------+----------+
1 row in set (0.00 sec)

4.Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output.

mysql> SELECT Passenger_name ,Boarding_City,Destination_city,p.Bus_Type, t.Price FROM passenger p, price t WHERE p.Distance = t.Distance and p.Bus_type = t.Bus_type;
+----------------+---------------+------------------+----------+-------+
| Passenger_name | Boarding_City | Destination_city | Bus_Type | Price |
+----------------+---------------+------------------+----------+-------+
| Sejal          | Bengaluru     | Chennai          | Sleeper  |   770 |
| Pallavi        | panaji        | Bengaluru        | Sleeper  |  1320 |
| Hemant         | panaji        | Mumbai           | Sleeper  |  1540 |
| Udit           | Trivandrum    | panaji           | Sleeper  |  2200 |
| Sejal          | Bengaluru     | Chennai          | Sleeper  |   434 |
| Manish         | Hyderabad     | Bengaluru        | Sitting  |   620 |
| Ankur          | Nagpur        | Hyderabad        | Sitting  |   620 |
| Manish         | Hyderabad     | Bengaluru        | Sitting  |   620 |
| Ankur          | Nagpur        | Hyderabad        | Sitting  |   620 |
| Piyush         | Pune          | Nagpur           | Sitting  |   868 |
| Anmol          | Mumbai        | Hyderabad        | Sitting  |   868 |
+----------------+---------------+------------------+----------+-------+
11 rows in set (0.00 sec)

5.What is the passenger name and his/her ticket price who travelled in Sitting bus  for a distance of 1000 KMs.

mysql> SELECT Passenger_name,p.Bus_type, Price FROM passenger p, price t WHERE p.Distance = 1000 and p.Bus_type = 'Sitting' and t.Distance = 1000 and t.Bus_type = 'Sitting';
Empty set (0.00 sec)

6.What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji?

mysql> select distinct Passenger_name, Boarding_city as Destination_city,Destination_city as Boarding_city, p.Bus_Type, t.Price from passenger p,price t where Passenger_name="Pallavi" and p.Distance=t.Distance;
+----------------+------------------+---------------+----------+-------+
| Passenger_name | Destination_city | Boarding_city | Bus_Type | Price |
+----------------+------------------+---------------+----------+-------+
| Pallavi        | panaji           | Bengaluru     | Sleeper  |  1320 |
| Pallavi        | panaji           | Bengaluru     | Sleeper  |   744 |
+----------------+------------------+---------------+----------+-------+
2 rows in set (0.00 sec)


7.List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order.

mysql> select distinct distance from passenger ORDER BY distance desc;
+----------+
| distance |
+----------+
|     1500 |
|     1000 |
|      700 |
|      600 |
|      500 |
|      350 |
+----------+
6 rows in set (0.00 sec)

8.Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables.

mysql> SELECT Passenger_name, Distance * 100.0/ (SELECT SUM(Distance) FROM passenger)FROM passenger GROUP BY Distance;
+----------------+---------------------------------------------------------+
| Passenger_name | Distance * 100.0/ (SELECT SUM(Distance) FROM passenger) |
+----------------+---------------------------------------------------------+
| Sejal          |                                                 5.34351 |
| Anmol          |                                                10.68702 |
| Pallavi        |                                                 9.16031 |
| Khusboo        |                                                22.90076 |
| Udit           |                                                15.26718 |
| Ankur          |                                                 7.63359 |
+----------------+---------------------------------------------------------+
6 rows in set (0.01 sec)

9.Create a view to see all passengers who travelled in AC Bus.

mysql> create VIEW p1 as SELECT * FROM Passenger WHERE Category = 'AC';
Query OK, 0 rows affected (0.02 sec)

mysql> select * from p1;
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+
5 rows in set (0.00 sec)

10.Create a stored procedure to find total passengers traveled using Sleeper buses.

mysql> delimiter //
mysql> create procedure sleeper_travellers()
    -> BEGIN
    -> select * from passenger where Bus_Type = 'sleeper';
    -> END
    -> //
Query OK, 0 rows affected (0.01 sec)

mysql> call sleeper_travellers();
    -> //
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
| Hemant         | Non-AC   | M      | panaji        | Mumbai           |      700 | Sleeper  |
+----------------+----------+--------+---------------+------------------+----------+----------+
5 rows in set (0.01 sec)

Query OK, 0 rows affected (0.03 sec)

11.Display 5 records at one time


mysql> Select * from passenger LIMIT 5;
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
+----------------+----------+--------+---------------+------------------+----------+----------+
5 rows in set (0.00 sec)


